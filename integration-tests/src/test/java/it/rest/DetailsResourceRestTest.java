package it.rest;

import com.atlassian.confluence.rest.client.authentication.AuthenticatedWebResourceProvider;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessRestTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.AttachmentFixture;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import static com.atlassian.confluence.it.RestHelper.doGetRequest;
import static com.atlassian.confluence.test.properties.TestProperties.DEFAULT_USERS_GROUP;
import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.VIEW;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests the DetailsResource in attachment macro.
 */
@RunWith(ConfluenceStatelessRestTestRunner.class)
public class DetailsResourceRestTest {

    private static final String IMAGE_NAME = "testlogo.jpg";
    private static final String TITLE = "Attachment macro page";

    @Inject
    private static ConfluenceRpcClient rpc;
    @Inject
    private static AuthenticatedWebResourceProvider restClientProvider;

    @Fixture
    private static UserFixture fullPermissionUser = UserFixture.userFixture()
            .existingGroup(DEFAULT_USERS_GROUP)
            .build();
    @Fixture
    private static UserFixture viewPermissionUser = UserFixture.userFixture()
            .existingGroup(DEFAULT_USERS_GROUP)
            .build();
    @Fixture
    private static UserFixture noPermissionUser = UserFixture.userFixture()
            .existingGroup(DEFAULT_USERS_GROUP)
            .build();
    @Fixture
    private static SpaceFixture space = SpaceFixture.spaceFixture()
            .keyPrefix("AttachmentPreviewTest")
            .permission(fullPermissionUser, SpacePermission.values())
            .permission(viewPermissionUser, VIEW)
            .build();
    @Fixture
    private static PageFixture page = PageFixture.pageFixture()
            .author(fullPermissionUser)
            .title(TITLE)
            .content("contents")
            .space(space)
            .build();
    @Fixture
    private static AttachmentFixture attachment = AttachmentFixture.attachmentFixture()
            .parent(page)
            .title(IMAGE_NAME)
            .contentType("image/jpg")
            .fromClasspath(IMAGE_NAME)
            .uploader(fullPermissionUser)
            .build();

    @Before
    public void setUp() {
        restClientProvider.clearAuthContext();

        rpc.getAdminSession()
                .getUserComponent()
                .removeUserFromGroup(noPermissionUser.get().getUsername(), DEFAULT_USERS_GROUP);
    }

    @After
    public void teardown() {
        restClientProvider.clearAuthContext();
    }

    @Test
    public void testAnonymousViewAttachmentWhenSpaceAllowAnonymous() {
        // Grant anonymous permissions on both global & space levels
        rpc.getAdminSession().getSystemComponent().enableAnonymousAccess();
        rpc.getAdminSession().getPermissionsComponent().grantAnonymousPermission(VIEW, space.get());

        ClientResponse response = getAttachmentPreview(attachment, null);

        try {
            assertEquals(200, response.getStatus());
            assertTrue(response.getEntity(String.class).contains(IMAGE_NAME));
        } finally {
            response.close();
            rpc.getAdminSession().getSystemComponent().disableAnonymousAccess();
            rpc.getAdminSession().getPermissionsComponent().revokeAnonymousPermission(VIEW, space.get());
        }
    }

    @Test
    public void testAnonymousViewAttachmentWhenSpaceNotAllowAnonymous() {
        // Revoke anonymous permissions on both global & space levels
        rpc.getAdminSession().getSystemComponent().disableAnonymousAccess();
        rpc.getAdminSession().getPermissionsComponent().revokeAnonymousPermission(VIEW, space.get());

        ClientResponse response = getAttachmentPreview(attachment, null);

        try {
            assertEquals(403, response.getStatus());
        } finally {
            response.close();
        }
    }

    @Test
    public void testUserViewAttachmentWhenHasViewPermission() {
        ClientResponse response = getAttachmentPreview(attachment, viewPermissionUser);

        try {
            assertEquals(200, response.getStatus());
            assertTrue(response.getEntity(String.class).contains(IMAGE_NAME));
        } finally {
            response.close();
        }
    }

    @Test
    public void testUserViewAttachmentWhenHasNoViewPermission() {
        ClientResponse response = getAttachmentPreview(attachment, noPermissionUser);

        try {
            assertEquals(403, response.getStatus());
        } finally {
            response.close();
        }
    }

    @Test
    public void testUserViewAttachmentWheAttachmentNotFound() {
        ClientResponse response = getAttachmentPreview(0L, viewPermissionUser);

        try {
            assertEquals(200, response.getStatus());
            assertEquals("", response.getEntity(String.class));
        } finally {
            response.close();
        }
    }

    private static void restLoginAs(final UserFixture user) {
        restClientProvider.setAuthContext(
                user.get().getUsername(),
                user.get().getPassword().toCharArray());
    }

    private ClientResponse getAttachmentPreview(AttachmentFixture attachment, @Nullable UserFixture user) {
        return getAttachmentPreview(attachment.get().getId().asLong(), user);
    }

    private ClientResponse getAttachmentPreview(long attachmentId, @Nullable UserFixture user) {
        if (user != null) {
            restLoginAs(user);
        }

        return doGetRequest(getWebResourceForAttachment(attachmentId), MediaType.TEXT_HTML);
    }

    private WebResource getWebResourceForAttachment(long attachmentId) {
        return restClientProvider.newRestWebResource()
                .path("rest/attachments/1.0/details/preview")
                .queryParam("attachmentId", String.valueOf(attachmentId));
    }
}