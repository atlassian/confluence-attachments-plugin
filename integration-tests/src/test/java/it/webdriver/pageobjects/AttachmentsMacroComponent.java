package it.webdriver.pageobjects;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.model.people.User;
import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.confluence.util.collections.Range.range;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AttachmentsMacroComponent extends ConfluenceAbstractPageComponent {
    private final Content page;

    public AttachmentsMacroComponent(Content page) {
        this.page = page;
    }

    @ElementBy(cssSelector = "table.attachments")
    PageElement attachmentsTable;

    @ElementBy(cssSelector = "table.attachments .filename-column a")
    PageElement filenameHeaderLink;

    @ElementBy(cssSelector = "table.attachments .modified-column a")
    PageElement modifiedHeaderLink;

    @ElementBy(cssSelector = ".download-all-link")
    PageElement downloadAllLink;

    @Inject
    JavascriptExecutor javascriptExecutor;

    @Init
    public void assertAttachmentsTableHeaders() {
        waitUntilTrue(filenameHeaderLink.timed().isVisible());
        waitUntilTrue(modifiedHeaderLink.timed().isVisible());
    }

    public PageElement getDownloadAllLink() {
        return downloadAllLink;
    }

    public URI getDownloadAllURL() throws URISyntaxException {
        return new URI(getDownloadAllLink().getAttribute("href"));
    }

    public AttachmentsMacroComponent clickSortByFilename() {
        filenameHeaderLink.click();
        return this;
    }

    public AttachmentsMacroComponent clickSortByModifiedDate() {
        modifiedHeaderLink.click();
        return this;
    }

    public AttachmentsMacroComponent assertAttachmentRowIsDisplayed(Content attachment, User lastModifiedBy) {
        getAttachmentRow(attachment).assertIsDisplayed(lastModifiedBy);
        return this;
    }

    public AttachmentsMacroComponent assertAttachmentRowIsNotDisplayed(Content attachment) {
        getAttachmentRow(attachment).assertIsNotVisible();
        return this;
    }

    public void assertAttachmentOrder(Content... expectedAttachmentOrder) {
        final List<Integer> expectedOrder = newArrayList(range(1, expectedAttachmentOrder.length + 1));
        waitUntilEquals("Unexpected attachment order", expectedOrder, getAttachmentOrder(expectedAttachmentOrder));
    }

    private TimedQuery<List<Integer>> getAttachmentOrder(final Content... attachments) {
        return Queries.forSupplier(new DefaultTimeouts(), () -> {
            final List<Integer> actualOrder = new ArrayList<>();
            for (int i = 0; i < attachments.length; i++) {
                Content attachment = attachments[i];
                final int idx = getAttachmentRow(attachment).getAttachmentIndex();
                actualOrder.add(idx);
            }
            return actualOrder;
        });
    }

    public AttachmentSummary openAttachmentSummary(Content attachment) {
        return toggleAttachmentSummary(attachment).getAttachmentSummary(attachment);
    }

    public AttachmentsMacroComponent toggleAttachmentSummary(Content attachment) {
        getAttachmentRow(attachment).getAttachmentSummaryToggle().click();
        return this;
    }

    /**
     * To test the logic of the file-upload callback (i.e. that the attachments are re-rendered correctly, we don't
     * actually have to upload a file.
     *
     * @param pageId             the page to refresh the attachments macro for
     * @param expectedAttachment an attachment to wait for after the refresh
     */
    public AttachmentsMacroComponent refreshAttachments(ContentId pageId, Content expectedAttachment) {
        String js = String.format("AJS.trigger('attachment-macro.refresh', %d);", pageId.asLong());
        javascriptExecutor.executeScript(js);

        final String expectedComment = expectedAttachment.getVersion().getMessage();

        waitUntilEquals(expectedComment, getUpdatedAttachmentComment(expectedComment));
        return this;
    }

    private TimedQuery<String> getUpdatedAttachmentComment(final String comment) {
        return Queries.forSupplier(new DefaultTimeouts(), () -> {
            List<WebElement> commentElements = driver.findElements(By.xpath(String.format("//span[contains(.,\"%s\")]", comment)));
            if (commentElements.isEmpty()) {
                return null; // no attachments yet, so no comments
            }
            return commentElements.get(0).getText();
        });
    }

    public AttachmentRow getAttachmentRow(Content attachment) {
        final PageElement row = attachmentsTable
                .find(By.cssSelector("tr[data-attachment-id='" + attachment.getId().asLong() + "']"));
        return pageBinder.bind(AttachmentRow.class, attachment, row);
    }

    public AttachmentSummary getAttachmentSummary(Content attachment) {
        return pageBinder.bind(
                AttachmentSummary.class,
                attachmentsTable.find(By.cssSelector("tr.attachment-summary-" + attachment.getId().asLong())),
                attachment, page);
    }

    public static class AttachmentRow extends ConfluenceAbstractPageComponent {
        private final Content attachment;
        private final PageElement container;

        public AttachmentRow(Content attachment, PageElement container) {
            this.attachment = attachment;
            this.container = container;
        }

        private PageElement getAttachmentSummaryToggle() {
            return container.find(By.cssSelector(".attachment-summary-toggle span"));
        }

        public PageElement getAttachmentDownloadLink() {
            return container.find(By.cssSelector("a.filename"));
        }

        public URI getAttachmentDownloadUrl() throws URISyntaxException {
            return new URI(getAttachmentDownloadLink().getAttribute("href"));
        }

        int getAttachmentIndex() {
            return Integer.parseInt(container.getAttribute("data-attachment-idx"));
        }

        public void assertIsNotVisible() {
            assertThat("Attachment [" + attachment.getTitle() + "] should not be present", container.isPresent(), is(false));
        }

        public void assertIsDisplayed(User lastModifiedBy) {
            final PageElement filenameLink = getAttachmentDownloadLink();
            final PageElement comment = container.find(By.className("attachment-comment"));
            final PageElement modified = container.find(By.className("attachment-created"));

            waitUntilEquals(attachment.getTitle(), filenameLink.timed().getText());
            waitUntilEquals(attachment.getVersion().getMessage(), comment.timed().getText());
            waitUntil(modified.timed().getText(), endsWith(lastModifiedBy.getDisplayName()));
        }
    }
}