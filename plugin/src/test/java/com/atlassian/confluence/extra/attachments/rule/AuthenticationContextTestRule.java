package com.atlassian.confluence.extra.attachments.rule;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * @since 8.2
 * </p>
 * Cleaning authentication context i.e. no authenticated user is in current thread
 */
public class AuthenticationContextTestRule extends TestWatcher {

    @Override
    protected void starting(Description description) {
        AuthenticatedUserThreadLocal.set(null);
    }

    @Override
    protected void finished(Description description) {
        AuthenticatedUserThreadLocal.set(null);
    }
}
