package com.atlassian.confluence.extra.attachments;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.extra.attachments.rule.AuthenticationContextTestRule;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.impl.DefaultUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * @since 8.2
 * </p>
 * Unit tests for SpaceAttachmentsMacro
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSpaceAttachmentsMacro {

    @Rule
    public AuthenticationContextTestRule authenticationContextTestRule = new AuthenticationContextTestRule();

    private Map<String, Object> contextMap;
    private List<Attachment> attachments;
    private String returnText = "Execution complete";

    @Mock
    private VelocityHelperService velocityHelperService;
    @Mock
    private SpaceAttachmentsUtils spaceAttachmentsUtils;
    @Mock
    private I18NBeanFactory i18NBeanFactory;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private FormatSettingsManager formatSettingsManager;
    @Mock
    private UserAccessor userAccessor;
    @Mock
    private Map<String, String> parameters;
    @Mock
    private PageContext pageContext;
    @Mock
    private SpaceAttachments spaceAttachments;
    @Mock
    private I18NBean i18NBean;

    @InjectMocks
    private SpaceAttachmentsMacro spaceAttachmentsMacro;

    @Before
    public void setUp() throws Exception {
        contextMap = new HashMap<>();

        attachments = createAttachments("file2.txt", "file1.txt", "file0.txt");

        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(contextMap);
        when(formatSettingsManager.getTimeFormat()).thenReturn("h:mm a");
        when(formatSettingsManager.getDateFormat()).thenReturn("MMM dd, yyyy");
        when(formatSettingsManager.getDateTimeFormat()).thenReturn("MMM dd, yyyy HH:mm");
        when(pageContext.getOutputDeviceType()).thenReturn("desktop");
    }

    @Test
    public void testAnonymousCanSeeAllAttachmentWhenSpaceHasPermission() throws MacroException, InvalidSearchException {
        String spaceKey = "ds";

        Space paramSpace = new Space();
        paramSpace.setKey(spaceKey);

        Page pageToBeRendered = new Page();
        pageToBeRendered.setId(1);
        pageToBeRendered.setTitle("Page to be Rendered");

        AuthenticatedUserThreadLocal.set(null);

        when(userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.get())).thenReturn(new ConfluenceUserPreferences());
        when(pageContext.getEntity()).thenReturn(pageToBeRendered);
        when(parameters.get("space")).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(paramSpace);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, paramSpace)).thenReturn(true);
        when(spaceAttachmentsUtils.getAttachmentList(spaceKey, 1, 0, 0, "date", null, null)).thenReturn(spaceAttachments);
        when(spaceAttachments.getTotalAttachments()).thenReturn(2);
        when(spaceAttachments.getAttachmentList()).thenReturn(attachments);
        when(spaceAttachments.getTotalPage()).thenReturn(1);
        when(velocityHelperService.getRenderedTemplate(anyString(), any(Map.class))).then(a -> {
            List listContext = (List) contextMap.get("latestVersionsOfAttachments");

            assertTrue(listContext.containsAll(attachments));
            assertEquals(listContext.stream().count(), attachments.stream().count());
            assertEquals(spaceKey, contextMap.get("spaceKey"));

            return returnText;
        });

        assertEquals(returnText, spaceAttachmentsMacro.execute(parameters, "", pageContext));
    }

    @Test
    public void testAnonymousCanNotSeeAnyAttachmentWhenSpaceHasNoPermission() throws MacroException {
        String spaceKey = "ds";

        Space paramSpace = new Space();
        paramSpace.setKey(spaceKey);

        Page pageToBeRendered = new Page();
        pageToBeRendered.setId(1);
        pageToBeRendered.setTitle("Page to be Rendered");

        AuthenticatedUserThreadLocal.set(null);

        String errorMessage = "User Anonymous does not have permission to view the space.";
        String expectedErrorMessage = "<div class=\"error\"><span class=\"error\">" + errorMessage + "</span> </div>";

        when(pageContext.getEntity()).thenReturn(pageToBeRendered);
        when(parameters.get("space")).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(paramSpace);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, paramSpace)).thenReturn(false);
        when(i18NBeanFactory.getI18NBean(any())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString(), any(String[].class))).thenReturn(errorMessage);

        assertEquals(expectedErrorMessage, spaceAttachmentsMacro.execute(parameters, "", pageContext));
    }

    @Test
    public void testAnyUserCanSeeAllAttachmentWhenSpaceHasPermission() throws MacroException, InvalidSearchException {
        String spaceKey = "ds";

        Space paramSpace = new Space();
        paramSpace.setKey(spaceKey);

        Page pageToBeRendered = new Page();
        pageToBeRendered.setId(1);
        pageToBeRendered.setTitle("Page to be Rendered");

        ConfluenceUser remoteUser = new ConfluenceUserImpl(new DefaultUser("testuser"));
        AuthenticatedUserThreadLocal.set(remoteUser);

        when(userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.get())).thenReturn(new ConfluenceUserPreferences());
        when(pageContext.getEntity()).thenReturn(pageToBeRendered);
        when(parameters.get("space")).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(paramSpace);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, paramSpace)).thenReturn(true);
        when(spaceAttachmentsUtils.getAttachmentList(spaceKey, 1, 0, 0, "date", null, null)).thenReturn(spaceAttachments);
        when(spaceAttachments.getTotalAttachments()).thenReturn(2);
        when(spaceAttachments.getAttachmentList()).thenReturn(attachments);
        when(spaceAttachments.getTotalPage()).thenReturn(1);
        when(velocityHelperService.getRenderedTemplate(anyString(), any(Map.class))).then(a -> {
            List listContext = (List) contextMap.get("latestVersionsOfAttachments");

            assertTrue(listContext.containsAll(attachments));
            assertEquals(listContext.stream().count(), attachments.stream().count());
            assertEquals(spaceKey, contextMap.get("spaceKey"));

            return returnText;
        });

        assertEquals(returnText, spaceAttachmentsMacro.execute(parameters, "", pageContext));
    }

    @Test
    public void testAuthenticatedUserCanNotSeeAnyAttachmentWhenSpaceHasNoPermission() throws MacroException {
        String spaceKey = "ds";

        Space paramSpace = new Space();
        paramSpace.setKey(spaceKey);

        Page pageToBeRendered = new Page();
        pageToBeRendered.setId(1);
        pageToBeRendered.setTitle("Page to be Rendered");

        ConfluenceUser remoteUser = new ConfluenceUserImpl(new DefaultUser("admin"));
        AuthenticatedUserThreadLocal.set(remoteUser);

        String errorMessage = "User testuser does not have permission to view the space.";
        String expectedErrorMessage = "<div class=\"error\"><span class=\"error\">" + errorMessage + "</span> </div>";

        when(pageContext.getEntity()).thenReturn(pageToBeRendered);
        when(parameters.get("space")).thenReturn(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(paramSpace);
        when(permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, paramSpace)).thenReturn(false);
        when(i18NBeanFactory.getI18NBean(any())).thenReturn(i18NBean);
        when(i18NBean.getText(anyString(), any(String[].class))).thenReturn(errorMessage);

        assertEquals(expectedErrorMessage, spaceAttachmentsMacro.execute(parameters, "", pageContext));
    }

    private static List<Attachment> createAttachments(String... names) {
        List<Attachment> result = new ArrayList<>();

        final Calendar now = Calendar.getInstance();
        for (String name : names) {
            Attachment attachment = createAttachment(name, 0, now.getTime());
            result.add(attachment);
            now.add(Calendar.DAY_OF_YEAR, 1);
        }

        return result;
    }

    private static Attachment createAttachment(final String name, final long size, final Date lastModificationDate) {
        final Attachment attachment = new Attachment(name, "text/plain", size, EMPTY);

        attachment.setId(name.hashCode());
        attachment.setLastModificationDate(lastModificationDate);
        return attachment;
    }
}
