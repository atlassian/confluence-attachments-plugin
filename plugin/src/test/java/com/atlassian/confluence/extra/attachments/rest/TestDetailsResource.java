package com.atlassian.confluence.extra.attachments.rest;

import com.atlassian.confluence.api.service.exceptions.PermissionException;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.attachments.ImagePreviewRenderer;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsResource {


    private DetailsResource resource;

    @Mock
    private I18NBeanFactory i18NBeanFactory;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private I18NBean i18NBean;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private ImagePreviewRenderer imagePreviewRenderer;
    @Mock
    private ConfluenceUser user;
    private Attachment attachment;
    private static final String previewHtml = "<p>rendered preview html</p>";

    @Before
    public void setUp() throws XhtmlException {
        attachment = new Attachment();
        attachment.setId(3141596L);
        attachment.setContainer(new Page());

        AuthenticatedUserThreadLocal.setUser(user);
        when(attachmentManager.getAttachment(attachment.getId())).thenReturn(attachment);
        when(localeManager.getLocale(user)).thenReturn(Locale.ENGLISH);
        when(i18NBeanFactory.getI18NBean(Locale.ENGLISH)).thenReturn(i18NBean);
        when(imagePreviewRenderer.render(eq(attachment), any(DefaultConversionContext.class))).thenReturn(previewHtml);
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), any(ContentEntityObject.class))).thenReturn(true);
        resource = new DetailsResource(attachmentManager, imagePreviewRenderer, permissionManager, i18NBeanFactory, localeManager);
    }

    @Test
    public void getPreviewShouldRaiseExceptionWhenUserHasNoViewPermission() throws PermissionException {
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), any(ContentEntityObject.class))).thenReturn(false);
        assertThrows(PermissionException.class, () -> resource.getPreview(attachment.getId()));
    }

    @Test
    public void getPreviewShouldReturnEmptyStringWhenNoAttachmentFound() {
        when(attachmentManager.getAttachment(attachment.getId())).thenReturn(null);

        String result = resource.getPreview(attachment.getId());
        assertEquals("", result);
    }

    @Test
    public void getPreviewShouldReturnPreviewHtmlWhenAttachmentFound() {
        String result = resource.getPreview(attachment.getId());
        assertEquals(previewHtml, result);
    }
}
