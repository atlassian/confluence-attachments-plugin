package com.atlassian.confluence.extra.attachments.rest;

import com.atlassian.confluence.api.service.exceptions.PermissionException;
import com.atlassian.confluence.api.service.exceptions.ServiceException;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.extra.attachments.ImagePreviewRenderer;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("details")
public class DetailsResource {
    private final AttachmentManager attachmentManager;
    private final ImagePreviewRenderer imagePreviewRenderer;
    private final PermissionManager permissionManager;
    private final I18NBeanFactory i18NBeanFactory;
    private final LocaleManager localeManager;

    public DetailsResource(
            @ComponentImport final AttachmentManager attachmentManager,
            final ImagePreviewRenderer imagePreviewRenderer,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final I18NBeanFactory i18NBeanFactory,
            @ComponentImport final LocaleManager localeManager) {
        this.attachmentManager = attachmentManager;
        this.imagePreviewRenderer = imagePreviewRenderer;
        this.permissionManager = permissionManager;
        this.i18NBeanFactory = i18NBeanFactory;
        this.localeManager = localeManager;
    }

    @Produces(MediaType.TEXT_HTML)
    @GET
    @Path("preview")
    @AnonymousAllowed
    public String getPreview(@QueryParam("attachmentId") long attachmentId) {
        final Attachment attachment = attachmentManager.getAttachment(attachmentId);
        if (attachment != null) {
            if (!hasViewPermission(attachment)) {
                throw new PermissionException(getI18NBean().getText("confluence.extra.attachments.error.noviewpermission"));
            }

            try {
                return getAttachmentPreviewHtml(attachment);
            } catch (XhtmlException e) {
                throw new ServiceException();
            }
        }
        return "";
    }

    private String getAttachmentPreviewHtml(Attachment attachment) throws XhtmlException {
        return imagePreviewRenderer.render(attachment, new DefaultConversionContext(attachment.getContainer().toPageContext()));
    }

    private boolean hasViewPermission(Attachment attachment) {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, attachment);
    }

    private I18NBean getI18NBean() {
        return i18NBeanFactory.getI18NBean(localeManager.getLocale(AuthenticatedUserThreadLocal.get()));
    }
}
