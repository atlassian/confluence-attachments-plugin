package com.atlassian.confluence.extra.attachments.rest.exception.mapper;

import com.atlassian.confluence.api.service.exceptions.ServiceException;
import com.atlassian.confluence.rest.api.exception.ServiceExceptionMapper;
import com.atlassian.confluence.rest.api.model.RestError;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Converts {@link ServiceException}s into {@link Response}s containing {@link RestError}s.
 * <p>
 * The conversion will be done automatically for any REST Resource that throws a ServiceException.
 * </p>
 *
 * @since 11.0.2
 */
@Provider
public class RestExceptionMapper extends ServiceExceptionMapper {
    @Override
    protected void _annotateThisClassWithProvider() {
        //NO-OP just gets the annotated class on the bundles class path https://jira.atlassian.com/browse/CRA-733
    }
}